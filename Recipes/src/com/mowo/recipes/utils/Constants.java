package com.mowo.recipes.utils;

public class Constants {
	public static final String API_KEY = "1FDhQgGAbgoAVRr4eUQJ2KhXzAkwemKs9LDbCdxklbXY6wU8rF";
	public static final String BASE_URL = "http://api.tumblr.com/v2/blog/";
	public static final String HOST = ".tumblr.com";
	public static final String PAGE_ITEM = "10";
	public static final String BASE_NAME = "foodffs";

	public static final String TAG_BACON = "bacon";
	public static final String TAG_BROWNIES = "brownies";
	public static final String TAG_CAKE = "cake";
	public static final String TAG_CARAMEL = "caramel";
	public static final String TAG_CHEESE = "cheese";
	public static final String TAG_CHEESECAKE = "cheesecake";
	public static final String TAG_CHOCOLATE = "chocolate";
}
