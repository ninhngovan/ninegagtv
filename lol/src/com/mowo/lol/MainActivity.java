package com.mowo.lol;

import com.ikimuhendis.ldrawer.ActionBarDrawerToggle;
import com.ikimuhendis.ldrawer.DrawerArrowDrawable;
import com.mowo.lol.base.BaseActivity;
import com.mowo.lol.fragment.GifsFragment;
import com.mowo.lol.utils.Constants;
import com.mowo.funcrarygifs.R;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@SuppressLint("NewApi")
public class MainActivity extends BaseActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerArrowDrawable drawerArrow;
	private ActionBar ab;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint({ "NewApi", "ResourceAsColor" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		ab = getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);

		drawerArrow = new DrawerArrowDrawable(this) {
			@Override
			public boolean isLayoutRtl() {
				return false;
			}
		};

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				drawerArrow, R.string.app_name, R.string.app_name) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();

		String[] values = new String[] { "Hot", "Gifs", "Fresh", "WTF", "Meme",
				"Share", "Rate" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		mDrawerList.setAdapter(adapter);
		mDrawerList.setOnItemClickListener(new HandlerClickItem());
		displayView(Constants.BASE_NAME_HOST);
	}

	@SuppressLint("ResourceAsColor")
	private class HandlerClickItem implements OnItemClickListener {

		@SuppressLint("ResourceAsColor")
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			switch (position) {
			case 0:
				displayView(Constants.BASE_NAME_HOST);
				mDrawerLayout.closeDrawer(mDrawerList);
				ab.setTitle("Hot");
				break;
			case 1:
				displayView(Constants.BASE_NAME);
				mDrawerLayout.closeDrawer(mDrawerList);
				ab.setTitle("Gifs");
				break;
			case 2:
				displayView(Constants.BASE_NAME_PHOTO);
				mDrawerLayout.closeDrawer(mDrawerList);
				ab.setTitle("Fresh");
				// mDrawerToggle.setAnimateEnabled(false);
				// drawerArrow.setProgress(1f);
				break;
			case 3:
				displayView(Constants.BASE_NAME_NINEGAG);
				mDrawerLayout.closeDrawer(mDrawerList);
				ab.setTitle("WTF");
				break;

			case 4:
				displayView(Constants.BASE_NAME_FUNNIEST);
				mDrawerLayout.closeDrawer(mDrawerList);
				ab.setTitle("Meme");
				break;

			case 5:
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");
				share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				share.putExtra(Intent.EXTRA_SUBJECT,
						getString(R.string.app_name));
				share.putExtra(
						Intent.EXTRA_TEXT,
						getString(R.string.app_description)
								+ "\n"
								+ "App : https://play.google.com/store/apps/details?id="
								+ getPackageName());
				startActivity(Intent.createChooser(share,
						getString(R.string.app_name)));
				break;
			case 6:
				String appUrl = "https://play.google.com/store/apps/details?id="
						+ getPackageName();
				Intent rateIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse(appUrl));
				startActivity(rateIntent);
				mDrawerLayout.closeDrawer(mDrawerList);
				break;
			}

		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void displayView(String tag) {
		Fragment fragment = GifsFragment.newInstance(tag);
		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();
		}
	}

}
