package com.custom.volley.interfaces;

public interface IKey {
    /*
     * **********************************************************************
     * Story List
     * **********************************************************************
     */
    public final String STORY_LIST_IMAGE                   = "featured_img";
    public final String STORY_LIST_ID                      = "id";
    public final String STORY_LIST_POST                    = "post";
    public final String STORY_LIST_POST_NAME               = "name";
    public final String STORY_LIST_POST_ID                 = "post_id";
    public final String STORY_LIST_POST_SLUG               = "slug";
    
    /*
     * **********************************************************************
     * Story Detail
     * **********************************************************************
     */
    public final String STORY_DETAIL_IMAGE                 = "featured_img";
    public final String STORY_DETAIL_AUTHOR                = "author";
    public final String STORY_DETAIL_CATEGORIES            = "categories";
    public final String STORY_DETAIL_CATEGORY_NAME         = "name";
    public final String STORY_DETAIL_POST                  = "post";
    public final String STORY_DETAIL_POST_NAME             = "name";
    public final String STORY_DETAIL_POST_SLUG             = "slug";
    public final String STORY_DETAIL_POST_CONTENT          = "content";
    public final String STORY_DETAIL_CHAPTERS              = "chapters";
    public final String STORY_DETAIL_CHAPTER_NO            = "no";
    public final String STORY_DETAIL_CHAPTER_POST          = "post";
    public final String STORY_DETAIL_CHAPTER_POST_CREAT_AT = "created_at";
    
    /*
     * **********************************************************************
     * Story Read
     * **********************************************************************
     */
    public final String STORY_READ_POST                    = "post";
    public final String STORY_READ_POST_CONTENT            = "content";
    
    /*
     * **********************************************************************
     * Setting
     * **********************************************************************
     */
    public final String SETTING_ID                         = "id";
    public final String SETTING_NAME                       = "name";
    
    /*
     * **********************************************************************
     * Story List By Category
     * **********************************************************************
     */
    public final String STORY_LIST_BY_STORIES              = "stories";
    public final String STORY_LIST_BY_STORIES_IMAGE        = "featured_img";
    public final String STORY_LIST_BY_STORIES_POST         = "post";
    public final String STORY_LIST_BY_STORIES_POST_NAME    = "name";
    public final String STORY_LIST_BY_STORIES_POST_ID      = "post_id";
    public final String STORY_LIST_BY_STORIES_POST_SLUG    = "slug";
}
