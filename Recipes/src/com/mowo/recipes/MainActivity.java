package com.mowo.recipes;

import com.ikimuhendis.ldrawer.ActionBarDrawerToggle;
import com.ikimuhendis.ldrawer.DrawerArrowDrawable;
import com.mowo.recipes.base.BaseActivity;
import com.mowo.recipes.fragment.RecipesFragment;
import com.mowo.recipes.utils.Constants;
import com.mowo.recipes.R;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@SuppressLint("NewApi")
public class MainActivity extends BaseActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerArrowDrawable drawerArrow;
	private ActionBar ab;
	private String[] values;
	private String[] content;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint({ "NewApi", "ResourceAsColor" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		ab = getActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.navdrawer);

		drawerArrow = new DrawerArrowDrawable(this) {
			@Override
			public boolean isLayoutRtl() {
				return false;
			}
		};

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				drawerArrow, R.string.app_name, R.string.app_name) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();

		values = new String[] { "Recipes", "Bacon", "Brownies", "Cakes",
				"Caramel", "Cheese", "Cheese Cake", "Chocolate", "Cinnamon",
				"Cookies", "Drinks", "Cupcakes", "Healthy", "Fruit",
				"Mushroom", "Meat", "Muffins", "Noodles", "Oreo", "Pancakes",
				"Pasta", "Peanut Butter", "Potato", "Sandwich", "Salad",
				"Salt", "Seafood", "Vanilla", "Soup", "Sugar", "Vegan",
				"Vegetarian", "Waffles" };

		content = new String[] { "", "bacon", "brownies", "cake", "caramel",
				"cheese", "cheesecake", "chocolate", "cinnamon", "cookies",
				"drinks", "cupcakes", "healthy", "fruit", "mushroom", "meat",
				"muffins", "noodles", "oreo", "pancakes", "pasta", "potato",
				"peanut%20butter", "sandwich", "salad", "salt", "seafood",
				"vanilla", "soup", "sugar", "vegan", "vegetarian", "waffles" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);
		mDrawerList.setAdapter(adapter);
		mDrawerList.setOnItemClickListener(new HandlerClickItem());
		displayView("");
	}

	@SuppressLint("ResourceAsColor")
	private class HandlerClickItem implements OnItemClickListener {

		@SuppressLint("ResourceAsColor")
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			displayView(content[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			ab.setTitle(values[position]);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_share).setVisible(!drawerOpen);
		menu.findItem(R.id.action_rate).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
			break;

		case R.id.action_share:
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("text/plain");
			share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
			share.putExtra(
					Intent.EXTRA_TEXT,
					getString(R.string.app_description)
							+ "\n"
							+ "App : https://play.google.com/store/apps/details?id="
							+ getPackageName());
			startActivity(Intent.createChooser(share,
					getString(R.string.app_name)));
			break;

		case R.id.action_rate:
			String appUrl = "https://play.google.com/store/apps/details?id="
					+ getPackageName();
			Intent rateIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(appUrl));
			startActivity(rateIntent);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void displayView(String tag) {
		Fragment fragment = RecipesFragment.newInstance(tag);
		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();
		}
	}

}
