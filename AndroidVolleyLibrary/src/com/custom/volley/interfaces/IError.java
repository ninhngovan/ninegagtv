package com.custom.volley.interfaces;

public interface IError {
    /*
     * **********************************************************************
     * Error common
     * *********************************************************************
     */
    public final String ERROR_OFFLINE_CONNECTION    = "Connection's lost";
    public final String ERROR_JSON_EMPTY            = "Json string's empty";
    public final String ERROR_NOT_ARRAY             = "Object's not array";
    public final String ERROR_ARRAY_CANT_FILTER     = "array can't filter";
    public final String ERROR_CANT_GET_VALUE_FILTER = "can't get array list after filter, need call filter before use";
    public final String ERROR_JSON_PARSER_CODE      = "Parse code from Json error";
    public final String ERROR_JSON_NO_CODE          = "Json's code isn't exists";
    public final String ERROR_FAIL_LOGIN            = "Login's error";
    public final String ERROR_CONFIG_EMPTY          = "Config file's empty data";
    public final String ERROR_URL_NOT_SET           = "url api's not set";
    public final String ERROR_EVENT_NULL            = "event in socket result's null";
    
    public final String ERROR_OUT_OF_INDEX          = "error out of index : %s - array size : %s";
    public final String ERROR_NULL_OF_VIEW          = "error null of view : %s";
    public final String ERROR_NULL_ROOT_VIEW        = "root view's null";
}
