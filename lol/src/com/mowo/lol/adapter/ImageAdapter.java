package com.mowo.lol.adapter;

import java.io.File;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.mowo.funcrarygifs.R;
import com.mowo.lol.object.ImageItem;
import com.mowo.lol.utils.TimeAgo;

public class ImageAdapter extends android.widget.BaseAdapter implements
		OnClickListener {
	private List<ImageItem> listImage;
	private Context context;
	private LayoutInflater inflater;
	private TimeAgo timeAgo;
	private Dialog dialog;

	public ImageAdapter(Context context, List<ImageItem> listImage) {
		this.context = context;
		this.listImage = listImage;
		this.inflater = LayoutInflater.from(context);
		timeAgo = new TimeAgo(context);
	}

	@Override
	public int getCount() {
		return listImage.size();
	}

	@Override
	public Object getItem(int position) {
		return listImage.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.image_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
			viewHolder.title = (TextView) convertView
					.findViewById(R.id.title_card);
			viewHolder.popupMenu = (ImageButton) convertView
					.findViewById(R.id.btn_submenu_feed);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.popupMenu.setOnClickListener(new PopUpMenu(position));
		String result = timeAgo.timeAgo(Long.valueOf(listImage.get(position)
				.getTimetamp()) * 1000);
		if (TextUtils.isEmpty(listImage.get(position).getCaption())) {
			viewHolder.title.setText(result);
		} else {
			viewHolder.title.setText(Html.fromHtml(Html.fromHtml(
					listImage.get(position).getCaption()).toString()));
		}
		Ion.with(context).load(listImage.get(position).getImageOrigin())
				.intoImageView(viewHolder.image);
		return convertView;
	}

	private class ViewHolder {
		public ImageView image;
		public TextView title;
		public ImageButton popupMenu;
	}

	@Override
	public void onClick(View v) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class PopUpMenu implements OnClickListener {
		private int position;

		public PopUpMenu(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			showDialog(position);
		}
	}

	private class DownloadListener implements OnClickListener {
		private int position;

		public DownloadListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			String url = listImage.get(position).getImageOrigin();
			if (!TextUtils.isEmpty(url)) {
				Toast.makeText(context, "Downloaded", Toast.LENGTH_LONG).show();
				downloadFile(url);
			} else {
				Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
			}

		}
	}

	private class ShareListener implements OnClickListener {
		private int position;

		public ShareListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			shareApp();
		}
	}

	private void downloadFile(String uRl) {
		File direct = new File(Environment.getExternalStorageDirectory()
				+ "/Mowo");

		if (!direct.exists()) {
			direct.mkdirs();
		}

		@SuppressWarnings("static-access")
		DownloadManager mgr = (DownloadManager) context
				.getSystemService(context.DOWNLOAD_SERVICE);

		Uri downloadUri = Uri.parse(uRl);
		DownloadManager.Request request = new DownloadManager.Request(
				downloadUri);

		request.setAllowedNetworkTypes(
				DownloadManager.Request.NETWORK_WIFI
						| DownloadManager.Request.NETWORK_MOBILE)
				.setAllowedOverRoaming(false)
				.setTitle("Wallpaper")
				.setDescription("downloading...")
				.setDestinationInExternalPublicDir("/WoGag",
						System.currentTimeMillis() + ".jpg");

		mgr.enqueue(request);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("InlinedApi")
	private void showDialog(int position) {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.R.color.transparent));
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		TextView share = (TextView) dialog.findViewById(R.id.share);
		TextView download = (TextView) dialog.findViewById(R.id.download);
		download.setOnClickListener(new DownloadListener(position));
		share.setOnClickListener(new ShareListener(position));
		dialog.show();
	}
	
	private void shareApp(){
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("text/plain");
		share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		share.putExtra(Intent.EXTRA_SUBJECT,
				context.getString(R.string.app_name));
		share.putExtra(
				Intent.EXTRA_TEXT,
				context.getString(R.string.app_description)
						+ "\n"
						+ "App : https://play.google.com/store/apps/details?id="
						+ context.getPackageName());
		context.startActivity(Intent.createChooser(share,
				context.getString(R.string.app_name)));
	}

}
