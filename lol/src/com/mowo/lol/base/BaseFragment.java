package com.mowo.lol.base;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;

import com.custom.volley.controller.RequestApiController;
import com.custom.volley.entity.ResultDTO;
import com.custom.volley.interfaces.IRequestController;

@SuppressLint("NewApi")
public class BaseFragment extends Fragment implements IRequestController {
	protected Handler handler = null;
	protected RequestApiController requestApiController = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			if (requestApiController == null) {
				requestApiController = new RequestApiController(getActivity(),
						this);
			}

			if (handler == null) {
				handler = new Handler();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(String message) throws Exception {
		// TODO Auto-generated method stub

	}

	public void getRequest(String url, String TAG, Object ObjectReference) {
		requestApiController.getRequest2(url, ObjectReference, TAG);
	}

}
