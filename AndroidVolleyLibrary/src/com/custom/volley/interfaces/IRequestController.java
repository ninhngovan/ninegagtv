package com.custom.volley.interfaces;

import com.custom.volley.entity.ResultDTO;

public interface IRequestController {
    public void onResponse(ResultDTO resultDTO) throws Exception;
    
    public void onError(String message) throws Exception;
}
