package com.mowo.recipes.adapter;

import java.io.File;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.mowo.recipes.R;
import com.mowo.recipes.object.RecipeItem;
import com.mowo.recipes.utils.TimeAgo;

public class RecipeAdapter extends android.widget.BaseAdapter implements
		OnClickListener {
	private List<RecipeItem> listRecipes;
	private Context context;
	private LayoutInflater inflater;
	private TimeAgo timeAgo;
	private Dialog dialog;

	public RecipeAdapter(Context context, List<RecipeItem> listImage) {
		this.context = context;
		this.listRecipes = listImage;
		this.inflater = LayoutInflater.from(context);
		timeAgo = new TimeAgo(context);
	}

	@Override
	public int getCount() {
		return listRecipes.size();
	}

	@Override
	public Object getItem(int position) {
		return listRecipes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.image_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
			viewHolder.title = (TextView) convertView
					.findViewById(R.id.title_card);
			viewHolder.popupMenu = (ImageButton) convertView
					.findViewById(R.id.btn_submenu_feed);
			viewHolder.source = (TextView) convertView
					.findViewById(R.id.source);
			viewHolder.loading = (LinearLayout) convertView
					.findViewById(R.id.layoutLoading);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.popupMenu.setOnClickListener(new Share(position));
		viewHolder.image.setOnClickListener(new ShowSource(position));
		String result = timeAgo.timeAgo(Long.valueOf(listRecipes.get(position)
				.getTimetamp()) * 1000);
		if (TextUtils.isEmpty(listRecipes.get(position).getCaption())) {
			viewHolder.title.setText(result);
		} else {
			String tittle = Html.fromHtml(
					listRecipes.get(position).getCaption()).toString();
			tittle = Html.fromHtml(tittle).toString();
			tittle = tittle.replace("Every hour. Show me what you cooked!", "");
			viewHolder.title.setText(tittle);
		}
		viewHolder.source.setText("Source: "
				+ listRecipes.get(position).getSourceTitle());
		viewHolder.source.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
		Ion.with(context).load(listRecipes.get(position).getImageOrigin())
				.intoImageView(viewHolder.image);
		viewHolder.loading.setVisibility(View.GONE);
		return convertView;
	}

	private class ViewHolder {
		public ImageView image;
		public TextView title;
		public TextView source;
		private LinearLayout loading;
		public ImageButton popupMenu;
	}

	@Override
	public void onClick(View v) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class Share implements OnClickListener {
		private int position;

		public Share(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			String tittle = Html.fromHtml(
					listRecipes.get(position).getCaption()).toString();
			tittle = Html.fromHtml(tittle).toString();
			tittle = tittle.replace("Every hour. Show me what you cooked!", "");
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("text/plain");
			share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			share.putExtra(Intent.EXTRA_SUBJECT,
					context.getString(R.string.app_name));
			share.putExtra(Intent.EXTRA_TEXT,
					tittle + "\n" + listRecipes.get(position).getSource());
			context.startActivity(Intent.createChooser(share,
					context.getString(R.string.app_name)));
		}
	}

	private class ShowSource implements OnClickListener {
		private int position;

		public ShowSource(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			try {
				// Intent intent = new Intent(context, RecipeDetail.class);
				// intent.putExtra(RecipeDetail.PARAM_VALUE,
				// listImage.get(position).getSource());
				// context.startActivity(intent);
				Intent browserIntent = new Intent("android.intent.action.VIEW",
						Uri.parse(listRecipes.get(position).getSource()));
				context.startActivity(browserIntent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class DownloadListener implements OnClickListener {
		private int position;

		public DownloadListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			String url = listRecipes.get(position).getImageOrigin();
			if (!TextUtils.isEmpty(url)) {
				Toast.makeText(context, "Downloaded", Toast.LENGTH_LONG).show();
				downloadFile(url);
			} else {
				Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
			}

		}
	}

	private class ShareListener implements OnClickListener {
		private int position;

		public ShareListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			dialog.dismiss();
			shareApp();
		}
	}

	private void downloadFile(String uRl) {
		File direct = new File(Environment.getExternalStorageDirectory()
				+ "/Mowo");

		if (!direct.exists()) {
			direct.mkdirs();
		}

		@SuppressWarnings("static-access")
		DownloadManager mgr = (DownloadManager) context
				.getSystemService(context.DOWNLOAD_SERVICE);

		Uri downloadUri = Uri.parse(uRl);
		DownloadManager.Request request = new DownloadManager.Request(
				downloadUri);

		request.setAllowedNetworkTypes(
				DownloadManager.Request.NETWORK_WIFI
						| DownloadManager.Request.NETWORK_MOBILE)
				.setAllowedOverRoaming(false)
				.setTitle("Wallpaper")
				.setDescription("downloading...")
				.setDestinationInExternalPublicDir("/WoGag",
						System.currentTimeMillis() + ".jpg");

		mgr.enqueue(request);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("InlinedApi")
	private void showDialog(int position) {
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.R.color.transparent));
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		TextView share = (TextView) dialog.findViewById(R.id.share);
		TextView download = (TextView) dialog.findViewById(R.id.download);
		download.setOnClickListener(new DownloadListener(position));
		share.setOnClickListener(new ShareListener(position));
		dialog.show();
	}

	private void shareApp() {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("text/plain");
		share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		share.putExtra(Intent.EXTRA_SUBJECT,
				context.getString(R.string.app_name));
		share.putExtra(
				Intent.EXTRA_TEXT,
				context.getString(R.string.app_description)
						+ "\n"
						+ "App : https://play.google.com/store/apps/details?id="
						+ context.getPackageName());
		context.startActivity(Intent.createChooser(share,
				context.getString(R.string.app_name)));
	}

}
