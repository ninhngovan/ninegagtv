package com.mowo.lol.object;

public class ImageItem {
	private String imageOrigin;
	private String imagePreview;
	private String caption;
	private String type;
	private String timetamp;

	public String getTimetamp() {
		return timetamp;
	}

	public void setTimetamp(String timetamp) {
		this.timetamp = timetamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getImageOrigin() {
		return imageOrigin;
	}

	public void setImageOrigin(String imageOrigin) {
		this.imageOrigin = imageOrigin;
	}

	public String getImagePreview() {
		return imagePreview;
	}

	public void setImagePreview(String imagePreview) {
		this.imagePreview = imagePreview;
	}
}
