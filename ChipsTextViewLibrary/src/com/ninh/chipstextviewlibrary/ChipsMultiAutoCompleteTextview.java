package com.ninh.chipstextviewlibrary;

import com.ninhnv.chipstextviewlibrary.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

public class ChipsMultiAutoCompleteTextview extends MultiAutoCompleteTextView
		implements OnItemClickListener {

	/*
	 * **********************************************************************
	 * Constructor
	 * *********************************************************************
	 */
	public ChipsMultiAutoCompleteTextview(Context context) {
		super(context);
		init(context);
	}

	public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	/*
	 * **********************************************************************
	 * Functions
	 * *********************************************************************
	 */

	public void init(Context context) {
		setOnItemClickListener(this);
		addTextChangedListener(new ChipsTextWatcher());
	}

	public void setChips() {
		if (getText().toString().contains(",")) {

			SpannableStringBuilder ssb = new SpannableStringBuilder(getText());
			String chips[] = getText().toString().trim().split(",");
			int x = 0;
			for (String c : chips) {
				LayoutInflater layoutInflater = (LayoutInflater) getContext()
						.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				TextView textView = (TextView) layoutInflater.inflate(
						R.layout.chips_textview, null);
				textView.setText(c);
				textView.setTextSize(16 * getResources().getDisplayMetrics().density);
				int spec = MeasureSpec.makeMeasureSpec(0,
						MeasureSpec.UNSPECIFIED);
				textView.measure(spec, spec);
				textView.layout(0, 0, textView.getMeasuredWidth(),
						textView.getMeasuredHeight());
				Bitmap b = Bitmap.createBitmap(textView.getWidth(),
						textView.getHeight(), Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(b);
				canvas.translate(-textView.getScrollX(), -textView.getScrollY());
				textView.draw(canvas);
				textView.setDrawingCacheEnabled(true);
				Bitmap cacheBmp = textView.getDrawingCache();
				Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
				textView.destroyDrawingCache();
				BitmapDrawable bmpDrawable = new BitmapDrawable(getResources(),
						viewBmp);
				bmpDrawable.setBounds(0, 0, bmpDrawable.getIntrinsicWidth(),
						bmpDrawable.getIntrinsicHeight());
				ssb.setSpan(new ImageSpan(bmpDrawable), x, x + c.length(),
						Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				x = x + c.length() + 1;
			}
			setText(ssb);
			setSelection(getText().length());
		}

	}

	/*
	 * **********************************************************************
	 * Event
	 * *********************************************************************
	 */

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		setChips();
	}

	/*
	 * **********************************************************************
	 * Inner Class
	 * *********************************************************************
	 */

	private class ChipsTextWatcher implements TextWatcher {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (count >= 1) {
				if (s.charAt(start) == ',')
					setChips();
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}

	}
}
