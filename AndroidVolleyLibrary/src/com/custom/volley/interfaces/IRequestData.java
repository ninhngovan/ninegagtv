package com.custom.volley.interfaces;

public interface IRequestData {
	/*
	 * **********************************************************************
	 * HOST
	 * **********************************************************************
	 */
	public final String HOST = "http://api.tumblr.com/v2/blog/photo-comments.tumblr.com";

	/*
	 * **********************************************************************
	 * Configure
	 * **********************************************************************
	 */
	public final String DEFAULT_PARAM_PAGE = "0";
	public final String DEFAULT_PER_PAGE = "10";
	public final String DEFAULT_ALL_IN_PAGE = "0";
	public final String API_KEY = "1FDhQgGAbgoAVRr4eUQJ2KhXzAkwemKs9LDbCdxklbXY6wU8rF";

	/*
	 * **********************************************************************
	 * API
	 * **********************************************************************
	 */
	public final String GET_POST_PHOTO_LIST = "/posts?api_key=" + API_KEY
			+ "&limit=" + DEFAULT_PER_PAGE + "&type=photo&offset="
			+ DEFAULT_PARAM_PAGE;

}
