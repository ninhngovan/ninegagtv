package com.mowo.recipes.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.custom.volley.entity.ResultDTO;
import com.custom.volley.utils.Logger;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.mowo.recipes.R;
import com.mowo.recipes.adapter.RecipeAdapter;
import com.mowo.recipes.base.BaseFragment;
import com.mowo.recipes.object.RecipeItem;
import com.mowo.recipes.utils.Constants;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

@SuppressLint("NewApi")
public class RecipesFragment extends BaseFragment {
	private final String TAG = RecipesFragment.class.getCanonicalName();
	private final static String PARAM_VALUE = "param_value";
	private List<RecipeItem> listRecipes;
	private RecipeAdapter recipeAdapter;
	private PullToRefreshListView listview;
	private int loadMore = 0;
	private LinearLayout layoutLoading;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.gifs_fragment, container,
				false);
		listview = (PullToRefreshListView) rootView
				.findViewById(R.id.timelineListview);
		layoutLoading = (LinearLayout) rootView
				.findViewById(R.id.layoutLoading);
		listRecipes = new ArrayList<>();
		recipeAdapter = new RecipeAdapter(getActivity(), listRecipes);
		listview.setAdapter(recipeAdapter);

		String name = getArguments().getString(PARAM_VALUE);
		getData(0, name);
		listview.setOnRefreshListener(new LoadMore(name));
		return rootView;
	}

	public static RecipesFragment newInstance(String value) {
		RecipesFragment f = new RecipesFragment();
		Bundle bundle = new Bundle();
		bundle.putString(PARAM_VALUE, value);
		f.setArguments(bundle);
		return f;
	}

	private class LoadMore implements OnRefreshListener2<ListView> {
		private String host;

		public LoadMore(String host) {
			this.host = host;
		}

		@Override
		public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
			listRecipes.clear();
			getData(0, host);
		}

		@Override
		public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
			loadMore = loadMore + 10;
			getData(loadMore, host);
		}
	}

	// call api
	private void getData(int page, String tag) {
		loadMore = page;
		String url;
		if (TextUtils.isEmpty(tag)) {
			url = Constants.BASE_URL + Constants.BASE_NAME + Constants.HOST
					+ "/posts?api_key=" + Constants.API_KEY + "&limit="
					+ Constants.PAGE_ITEM + "&offset=" + String.valueOf(page);
		} else {
			url = Constants.BASE_URL + Constants.BASE_NAME + Constants.HOST
					+ "/posts?api_key=" + Constants.API_KEY + "&limit="
					+ Constants.PAGE_ITEM + "&offset=" + String.valueOf(page)
					+ "&tag=" + tag;
		}
		Logger.i(TAG, "ninh: " + url);
		if (isNetworkConnected()) {
			getRequest(url, TAG, null);
		} else {
			try {
				simpleError("Internet Connection Is Required");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	// response
	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {
		super.onResponse(resultDTO);
		try {
			layoutLoading.setVisibility(View.GONE);
			listview.onRefreshComplete();
			JSONArray arrPost = (JSONArray) resultDTO.clear().key("response")
					.key("posts").value();
			int size = arrPost.length();
			for (int i = 0; i < size; i++) {
				Logger.i("ninh", "data: : " + arrPost.getJSONObject(i));
				RecipeItem item = new RecipeItem();
				String type = (String) resultDTO.clear().key("response")
						.key("posts").key("type", "" + i).value();
				String caption = (String) resultDTO.clear().key("response")
						.key("posts").key("caption", "" + i).value();
				String timetamp = (String) resultDTO.clear().key("response")
						.key("posts").key("timestamp", "" + i).value();
				String source = "";
				try {
					source = (String) resultDTO.clear().key("response")
							.key("posts").key("source_url", "" + i).value();
				} catch (Exception e) {
					source = "Collecting";
				}

				Logger.i("ninh", "source: " + source);
				String sourceTitle = "";
				try {
					sourceTitle = (String) resultDTO.clear().key("response")
							.key("posts").key("source_title", "" + i).value();
				} catch (Exception e) {
					sourceTitle = "Collecting";
				}
				if (type.equals("photo")) {
					JSONObject originalSize = (JSONObject) resultDTO.clear()
							.key("response").key("posts").key("photos", "" + i)
							.key("original_size", "" + 0).value();
					String imageOrigin = originalSize.getString("url");
					item.setImageOrigin(imageOrigin);

					JSONArray previewSize = (JSONArray) resultDTO.clear()
							.key("response").key("posts").key("photos", "" + i)
							.key("alt_sizes", "" + 0).value();
					if (previewSize.length() > 2) {
						String imagePreview = previewSize.getJSONObject(2)
								.getString("url");
						item.setImagePreview(imagePreview);
					} else {
						String imagePreview2 = previewSize.getJSONObject(0)
								.getString("url");
						item.setImagePreview(imagePreview2);
					}

				}
				item.setSourceTitle(sourceTitle);
				item.setSource(source);
				item.setCaption(caption);
				item.setType(type);
				item.setTimetamp(timetamp);
				listRecipes.add(item);
				recipeAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getRequest(String url, String TAG, Object ObjectReference) {
		super.getRequest(url, TAG, ObjectReference);
	}

	private boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm.getActiveNetworkInfo() != null)
				&& cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	public void simpleError(String msg) throws Exception {

		Builder builder = new Builder(getActivity());
		builder.setMessage(msg);

		builder.setCancelable(true);
		builder.setNegativeButton("Exit",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						getActivity().finish();
					}
				});
		builder.show();
	}

}
