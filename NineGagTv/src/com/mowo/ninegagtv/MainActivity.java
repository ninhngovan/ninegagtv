package com.mowo.ninegagtv;

import com.custom.volley.entity.ResultDTO;
import com.custom.volley.utils.Logger;
import com.mowo.ninegagtv.base.BaseActivity;
import com.mowo.ninegagtv.utils.Constants;

import android.os.Bundle;

public class MainActivity extends BaseActivity {

	private String TAG = MainActivity.class.getCanonicalName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getData(0);
	}

	private void getData(int page) {
		String url = Constants.BASE_URL + Constants.HOST + "/posts?api_key="
				+ Constants.API_KEY + "&limit=" + Constants.PAGE_ITEM
				+ "&offset=" + String.valueOf(page);
		getRequest(url, TAG, null);
	}

	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {
		super.onResponse(resultDTO);
		try {
			Logger.i("ninh", "---: " + resultDTO.object.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getRequest(String url, String TAG, Object ObjectReference) {
		super.getRequest(url, TAG, ObjectReference);
	}

}
