package com.mowo.ninegagtv.base;

import com.custom.volley.controller.RequestApiController;
import com.custom.volley.entity.ResultDTO;
import com.custom.volley.interfaces.IRequestController;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

public class BaseActivity extends Activity implements IRequestController {

	protected Handler handler = null;
	protected RequestApiController requestApiController = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			if (requestApiController == null) {
				requestApiController = new RequestApiController(this, this);
			}

			if (handler == null) {
				handler = new Handler();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {

	}

	@Override
	public void onError(String message) throws Exception {

	}

	public void getRequest(String url, String TAG, Object ObjectReference) {
		requestApiController.getRequest2(url, ObjectReference, TAG);
	}

}
