package com.custom.volley.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import com.custom.volley.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class Utils {
	/*
	 * **********************************************************************
	 * Variables
	 * *********************************************************************
	 */
	public final static String TAG = Utils.class.getSimpleName();

	/*
	 * **********************************************************************
	 * Window
	 * *********************************************************************
	 */
	private static DisplayMetrics getDisplayMetrics(Activity activity)
			throws Exception {
		DisplayMetrics metrics = new DisplayMetrics();
		Display display = activity.getWindowManager().getDefaultDisplay();
		display.getMetrics(metrics);
		return metrics;
	}

	public static int getScreenWidth(Activity activity) throws Exception {
		return getDisplayMetrics(activity).widthPixels;
	}

	/*
	 * **********************************************************************
	 * Validate
	 * *********************************************************************
	 */

	public static boolean checkConnect(Context context) throws Exception {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (manager == null) {
			return false;
		}
		boolean is3g = false;
		if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null) {
			is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.isConnectedOrConnecting();
		}
		boolean isWifi = false;
		if (manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null) {
			isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
					.isConnectedOrConnecting();
		}
		if (!is3g && !isWifi)
			return false;
		return true;
	}

	/**
	 * Check application's installed
	 * 
	 * @param activity
	 * @param uri
	 *            : package name
	 * @return true: installed; false: not
	 */
	public static boolean checkAppInstalledOrNot(Activity activity, String uri)
			throws Exception {
		if (activity == null || uri == null || uri.equals("")) {
			return false;
		}
		PackageManager pm = activity.getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		} catch (Exception e) {
			app_installed = false;
		}
		return app_installed;
	}

	/**
	 * Check empty of string
	 * 
	 * @param string
	 *            the string need to check empty
	 * @return true if string equals null, "", "null", false otherwise
	 */
	public static boolean isEmpty(final String string) throws Exception {
		try {
			return ((string == null) || "".equals(string) || "null"
					.equals(string));
		} catch (final Exception e) {
			return true;
		}
	}

	/**
	 * Validate position of array
	 * 
	 * @param position
	 * @param viewRoots
	 * @return
	 */
	public static boolean validatePosition(int position,
			ArrayList<View> viewRoots) throws Exception {
		if (position >= 0 && position < viewRoots.size()) {
			return true;
		}
		return false;
	}

	/*
	 * **********************************************************************
	 * File
	 * *********************************************************************
	 */
	public static String loadDataFromAssetFile(Context context, String file)
			throws Exception {
		String tContents = "";

		try {
			InputStream stream = context.getAssets().open(file);

			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);
			stream.close();
			tContents = new String(buffer);
		} catch (IOException e) {
		}

		return tContents;
	}

	/*
	 * **********************************************************************
	 * Notification
	 * *********************************************************************
	 */
	public static void ringMessager(Context context) throws Exception {
		Uri notification = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Ringtone r = RingtoneManager.getRingtone(context, notification);
		r.play();
	}

	public static void notifyToast(Context context, String messenger)
			throws Exception {
		notifyToast(context, messenger, Toast.LENGTH_SHORT);
	}

	public static void notifyToast(Context context, String messenger,
			int duration) throws Exception {
		Toast.makeText(context, messenger, duration).show();
	}

	/*
	 * **********************************************************************
	 * Keyboard
	 * *********************************************************************
	 */
	/**
	 * Show/Hide soft keyboard
	 * 
	 * @author NoeLove
	 * @param activity
	 * @param isShowKeyBoard
	 * @throws Exception
	 */
	public static void showHideKeyboard(Activity activity,
			boolean isShowKeyBoard) throws Exception {
		Window window = activity.getWindow();
		if (!isShowKeyBoard) {
			if (window.getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) {
				window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			}
		} else {
			if (window.getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
				window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			}
		}
	}

	/**
	 * Hide keyboard
	 * 
	 * @param activity
	 * @author NoeLove
	 * @throws Exception
	 */
	public static void isCloseKeyboard(Activity activity) throws Exception {
		try {
			InputMethodManager inputManager = (InputMethodManager) activity
					.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
					.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * **********************************************************************
	 * TimeStamp
	 * *********************************************************************
	 */
	/**
	 * ConvertDateToTimeStamp
	 * 
	 * @param date
	 * @return
	 */
	public static long convertDateToTimeStamp(String date) throws Exception {
		Date startDate;
		try {
			SimpleDateFormat df = new SimpleDateFormat("hh:mm dd/MM/yyyy",
					Locale.getDefault());
			startDate = df.parse(date);
			return startDate.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * GetDateFromTimeStamp
	 * 
	 * @param timestamp
	 * @return
	 * @throws ParseException
	 */
	public static String getDateFromTimeStamp(long timestamp)
			throws ParseException {
		Date startDate;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",
				Locale.getDefault());
		startDate = new Date(timestamp);
		String newDateString = df.format(startDate);
		return newDateString;
	}

	/**
	 * GetDateFromTimeStamp by format "HH:mm:ss - dd/MM/yyyy"
	 * 
	 * @param timestamp
	 * @return
	 * @throws ParseException
	 */
	public static String getDate(long milliSeconds, String dateFormat) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	/**
	 * GetDateFromString
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateFromString(String date) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",
				Locale.getDefault());
		Date startDate;
		try {
			startDate = df.parse(date);
			String newDateString = df.format(startDate);
			return newDateString;
		} catch (ParseException e) {
			e.printStackTrace();
			return "error";
		}
	}

	/**
	 * convert date String to milisecond
	 * 
	 * @param date
	 *            : format dd/MM/yyyy HH:mm:ss
	 * @return
	 */
	public static long convertStringDateToMilliSecond(String date) {
		long milliSecond = 0;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			formatter.setLenient(false);

			Date oldDate = formatter.parse(date);
			milliSecond = oldDate.getTime();
			milliSecond /= 1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return milliSecond;
	}

	/**
	 * convert long time to string time
	 * 
	 * @return String time
	 */
	static long seconds, min, hours, days, diff;

	public static String getDifferentTimeNew(long timeAfter, long timeCurrent) {
		String txtResult = "";
		diff = timeCurrent - timeAfter;
		if (diff < 0) {
			txtResult = "vÃ i giÃ¢y trÆ°á»›c";
		} else {
			seconds = diff / 1000 % 60;
			min = diff / (60 * 1000) % 60;
			hours = diff / (60 * 60 * 1000) % 24;
			days = diff / (24 * 60 * 60 * 1000);

			// MyLog.printInfo("--S--"+mSimpleDateFormat.format(serverDate));
			// MyLog.printInfo("--C--"+mSimpleDateFormat.format(clientDate));
			// MyLog.printInfo("DIFF is "+days+"days, "+hours+"hours, "+min+"min, "+seconds+"second");

			if (!String.valueOf(days).equalsIgnoreCase("0")) {
				txtResult = days + " ngÃ y trÆ°á»›c";
			} else if (!String.valueOf(hours).equalsIgnoreCase("0")) {
				txtResult = hours + " giá»� trÆ°á»›c";
			} else if (!String.valueOf(min).equalsIgnoreCase("0")) {
				txtResult = min + " phÃºt trÆ°á»›c";
			} else if (!String.valueOf(seconds).equalsIgnoreCase("0")) {
				txtResult = seconds + " giÃ¢y trÆ°á»›c";
			} else {
			}
		}
		return txtResult;
	}

	/*
	 * **********************************************************************
	 * Recycle
	 * *********************************************************************
	 */
	public static void clearHandler(Handler... handlers) throws Exception {
		if (handlers == null) {
			return;
		}
		if (handlers.length <= 0) {
			return;
		}
		for (Handler handler : handlers) {
			if (handler != null) {
				handler.removeCallbacksAndMessages(null);
			}
		}
	}

	/*
	 * **********************************************************************
	 * Fragment
	 * *********************************************************************
	 */

	/*
	 * **********************************************************************
	 * JSON Parser
	 * *********************************************************************
	 */
	public final static String getStringFromJson(Object object, String key,
			String... params) throws Exception {
		String result = "";
		try {
			if (object instanceof JSONObject) {
				result = ((JSONObject) object).opt(key).toString();
			} else if (object instanceof JSONArray) {
				JSONObject buffer = (JSONObject) ((JSONArray) object)
						.get(Integer.parseInt(params[0]));
				result = buffer.opt(key).toString();
			}
		} catch (Exception e) {
		}
		return result;
	}

	/*
	 * **********************************************************************
	 * Other
	 * *********************************************************************
	 */
	public static void expandAll(ExpandableListView listView) throws Exception {
		for (int i = 0; i < listView.getExpandableListAdapter().getGroupCount(); i++) {
			listView.expandGroup(i);
		}
	}

	public static void closeKeyboard(Context context, EditText searchEditText) {
		((InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
	}

	public static void openKeyboard(Context context, EditText searchEditText) {
		((InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
				searchEditText, InputMethodManager.SHOW_FORCED);
	}

	@SuppressLint("NewApi")
	public static void downloadFile(String uRl, Context context, String title,
			String folder) {
		File direct = new File(Environment.getExternalStorageDirectory()
				+ "/Mowo");

		if (!direct.exists()) {
			direct.mkdirs();
		}

		@SuppressWarnings("static-access")
		DownloadManager mgr = (DownloadManager) context
				.getSystemService(context.DOWNLOAD_SERVICE);

		Uri downloadUri = Uri.parse(uRl);
		DownloadManager.Request request = new DownloadManager.Request(
				downloadUri);

		request.setAllowedNetworkTypes(
				DownloadManager.Request.NETWORK_WIFI
						| DownloadManager.Request.NETWORK_MOBILE)
				.setAllowedOverRoaming(false)
				.setTitle(title)
				.setDescription("downloading...")
				.setDestinationInExternalPublicDir("/folder",
						System.currentTimeMillis() + ".jpg");

		mgr.enqueue(request);
	}
}
