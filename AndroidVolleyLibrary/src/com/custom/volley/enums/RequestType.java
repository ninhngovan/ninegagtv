package com.custom.volley.enums;

import com.android.volley.Request;

public enum RequestType {
    GET(Request.Method.GET), POST(Request.Method.POST);
    
    private int method = 0;
    
    private RequestType(int method) {
        this.method = method;
    }
    
    public int getType() {
        return method;
    }
}
