package com.custom.volley.controller;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.custom.volley.entity.ResultDTO;
import com.custom.volley.enums.ActionType;
import com.custom.volley.interfaces.IRequestController;

public class RequestApiController {
	private RequestQueue mVolleyQueue;
	private JsonObjectRequest jsonObjRequest;
	private IRequestController iRequestController;

	public RequestApiController(Context context,
			IRequestController iRequestController) {
		this.iRequestController = iRequestController;
		mVolleyQueue = Volley.newRequestQueue(context);
	}

	public void getRequest(String url, ActionType actionType, Object data,
			String tag) {
		jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new OnResponse(actionType, data), new OnErrorRespon());
		/*
		 * Set a retry policy in case of SocketTimeout & ConnectionTimeout
		 * Exceptions. Volley does retry for you if you have specified the
		 * policy.
		 */
		jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		jsonObjRequest.setTag(tag);
		mVolleyQueue.add(jsonObjRequest);
		mVolleyQueue.getCache().clear();
	}

	public void getRequest2(String url, Object data, String tag) {
		jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new OnResponse(data), new OnErrorRespon());
		/*
		 * Set a retry policy in case of SocketTimeout & ConnectionTimeout
		 * Exceptions. Volley does retry for you if you have specified the
		 * policy.
		 */
		jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		jsonObjRequest.setTag(tag);
		mVolleyQueue.add(jsonObjRequest);
		mVolleyQueue.getCache().clear();
	}

	private class OnResponse implements Response.Listener<JSONObject> {
		private ActionType actionType = ActionType.DO_NOTHING;
		private Object data = null;

		public OnResponse(ActionType actionType, Object data) {
			this.actionType = actionType;
			this.data = data;
		}

		public OnResponse(Object data) {
			this.data = data;
		}

		@Override
		public void onResponse(JSONObject response) {
			try {
				ResultDTO resultDTO = new ResultDTO(response.toString());
				// resultDTO.object = response;
				resultDTO.actionType = actionType;
				resultDTO.referenceData = data;

				if (iRequestController != null) {
					iRequestController.onResponse(resultDTO);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class OnErrorRespon implements Response.ErrorListener {

		@Override
		public void onErrorResponse(VolleyError error) {
			/*
			 * Handle your error types accordingly.For Timeout & No connection
			 * error, you can show 'retry' button. For AuthFailure, you can re
			 * login with user credentials. For ClientError, 400 & 401, Errors
			 * happening on client side when sending api request. In this case
			 * you can check how client is forming the api and debug
			 * accordingly. For ServerError 5xx, you can do retry or handle
			 * accordingly.
			 */
			if (error instanceof NetworkError) {
			} else if (error instanceof ClientError) {
			} else if (error instanceof ServerError) {
			} else if (error instanceof AuthFailureError) {
			} else if (error instanceof ParseError) {
			} else if (error instanceof NoConnectionError) {
			} else if (error instanceof TimeoutError) {
			}
		}
	}
}
