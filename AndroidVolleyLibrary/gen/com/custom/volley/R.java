/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.custom.volley;

public final class R {
    public static final class anim {
        public static int fade_in=0x7f030000;
    }
    public static final class attr {
    }
    public static final class color {
        public static int action_bar=0x7f040009;
        public static int action_bar_title=0x7f04000a;
        public static int black=0x7f040006;
        public static int counter_text_bg=0x7f040004;
        public static int counter_text_color=0x7f040005;
        public static int grid_bg=0x7f040007;
        public static int grid_item_bg=0x7f040008;
        public static int list_background=0x7f040001;
        public static int list_background_pressed=0x7f040002;
        public static int list_divider=0x7f040003;
        public static int list_item_title=0x7f040000;
        public static int settings_label=0x7f04000c;
        public static int white=0x7f04000b;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static int activity_horizontal_margin=0x7f050000;
        public static int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static int abc_ic_go_search_api_mtrl_alpha=0x7f020000;
        public static int abc_list_focused_holo=0x7f020001;
        public static int abc_list_longpressed_holo=0x7f020002;
        public static int abc_list_pressed_holo_dark=0x7f020003;
        public static int abc_list_pressed_holo_light=0x7f020004;
        public static int abc_list_selector_disabled_holo_dark=0x7f020005;
        public static int abc_list_selector_disabled_holo_light=0x7f020006;
        public static int abs__ic_menu_moreoverflow_normal_holo_dark=0x7f020007;
        public static int bg_90alpha=0x7f020008;
        public static int bg_control_mid=0x7f020009;
        public static int bg_control_mid_pressed=0x7f02000a;
        public static int bg_footer=0x7f02000b;
        public static int bg_footer_bar=0x7f02000c;
        public static int bg_header_01=0x7f02000d;
        public static int bg_panel_white=0x7f02000e;
        public static int box_middle=0x7f02000f;
        public static int btn_navigation_back=0x7f020010;
        public static int btn_navigation_back_press=0x7f020011;
        public static int ic_action_overflow=0x7f020012;
        public static int ic_drawer=0x7f020013;
        public static int ic_game_gold=0x7f020014;
        public static int ic_game_silver=0x7f020015;
        public static int ic_home=0x7f020016;
        public static int ic_hot=0x7f020017;
        public static int ic_launcher=0x7f020018;
        public static int ic_like_n=0x7f020019;
        public static int ic_like_o=0x7f02001a;
        public static int ic_share_24=0x7f02001b;
        public static int ico_btn_like=0x7f02001c;
        public static int ico_btn_unlike=0x7f02001d;
        public static int ico_light=0x7f02001e;
        public static int splash=0x7f02001f;
    }
    public static final class id {
        public static int action_settings=0x7f090000;
    }
    public static final class integer {
        public static int grid_column_count=0x7f060000;
    }
    public static final class menu {
        public static int main=0x7f080000;
    }
    public static final class string {
        public static int action_settings=0x7f070002;
        public static int app_name=0x7f070000;
        public static int description=0x7f070001;
        public static int download_wallpaper=0x7f07000a;
        public static int lbl_btn_save=0x7f070013;
        public static int lbl_gallery_name=0x7f070012;
        public static int lbl_google_username=0x7f070010;
        public static int lbl_no_grid_columns=0x7f070011;
        public static int msg_unknown_error=0x7f07000b;
        public static int msg_wall_fetch_error=0x7f07000c;
        public static int nav_drawer_recently_added=0x7f070003;
        public static int set_wallpaper=0x7f070009;
        public static int splash_error=0x7f070008;
        public static int toast_enter_gallery_name=0x7f07000f;
        public static int toast_enter_google_username=0x7f07000d;
        public static int toast_enter_valid_grid_columns=0x7f07000e;
        public static int toast_saved=0x7f070004;
        public static int toast_saved_failed=0x7f070005;
        public static int toast_wallpaper_set=0x7f070006;
        public static int toast_wallpaper_set_failed=0x7f070007;
    }
}
