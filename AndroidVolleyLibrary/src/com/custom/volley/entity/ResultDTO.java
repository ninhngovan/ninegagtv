package com.custom.volley.entity;

import com.custom.volley.enums.ActionType;
import com.custom.volley.interfaces.IError;
import com.custom.volley.utils.Utils;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ResultDTO extends BaseDTO implements IError {
    /*
     * **********************************************************************
     * Variables
     * **********************************************************************
     */
    public ActionType         actionType    = ActionType.DO_NOTHING;
    public Object             referenceData = null;
    
    public String             tag           = null;
    public String             code          = null;
    public String             messenger     = null;
    
    private ArrayList<String> keys          = null;
    
    /**
     * Object store JSON result
     */
    public Object             object        = null;
    
    /**
     * Object store JSON position after get by key
     */
    private Object            buffer        = null;
    
    /**
     * Object after filter from JSON result
     */
    private Object            filter        = null;
    
    /*
     * **********************************************************************
     * Constructor
     * **********************************************************************
     */
    public ResultDTO(String jsonString) throws Exception {
        convert(jsonString);
        if (keys == null) {
            keys = new ArrayList<String>();
        }
    }
    
    /*
     * **********************************************************************
     * Action
     * **********************************************************************
     */
    /**
     * Move to root of JSON
     * 
     * @return
     * @throws Exception
     */
    public ResultDTO clear() throws Exception {
        return clear(null);
    }
    
    /**
     * Move to object position in JSON
     * 
     * @return
     * @throws Exception
     */
    public ResultDTO clear(Object object) throws Exception {
        if (object == null) {
            buffer = null;
        } else {
            buffer = object;
        }
        keys.clear();
        return this;
    }
    
    /**
     * Move to key in current position
     * 
     * @param key
     * @param params
     *            if current Object's JSONArray, the first parameter's position
     * @return
     * @throws Exception
     */
    public ResultDTO key(String key, String... params) throws Exception {
        Object root = getRoot();
        if (root instanceof JSONObject) {
            buffer = ((JSONObject) root).opt(key);
            keys.add(key);
        } else if (root instanceof JSONArray) {
            buffer = ((JSONObject) ((JSONArray) root).get(Integer.parseInt(params[0]))).opt(key);
            keys.add(key);
        } else {
        }
        return this;
    }
    
    /**
     * Get value of key in current position (without filter data)
     * 
     * @return
     * @throws Exception
     */
    public Object value() throws Exception {
        return value(false);
    }
    
    /**
     * Get value of key in current position
     * 
     * @param isFilter
     * @return
     * @throws Exception
     */
    public Object value(boolean isFilter) throws Exception {
        if (isFilter) {
            if (filter == null) {
                throw new Exception(ERROR_CANT_GET_VALUE_FILTER);
            } else {
                return validateReturnObject(filter);
            }
        } else {
            Object root = getRoot();
            if (root == null) {
                return null;
            }
            return validateReturnObject(root);
        }
    }
    
    /**
     * Put value to key in current Object
     * 
     * @param key
     * @param value
     * @param params
     *            if current Object's JSONArray, the first parameter's position
     * @throws Exception
     */
    public void put(String key, Object value, String... params) throws Exception {
        Object root = getRoot();
        if (root instanceof JSONObject) {
            if (buffer == null) {
                object = ((JSONObject) root).putOpt(key, value);
            } else {
                buffer = ((JSONObject) root).putOpt(key, value);
            }
        } else if (root instanceof JSONArray) {
            String position = null;
            try {
                position = params[0];
            } catch (Exception e) {
            }
            
            if (buffer == null) {
                if (Utils.isEmpty(key)) {
                    if (Utils.isEmpty(position)) {
                        object = ((JSONArray) root).put(value);
                    } else {
                        object = ((JSONArray) root).put(Integer.parseInt(position), value);
                    }
                } else {
                    object = ((JSONObject) ((JSONArray) root).get(Integer.parseInt(position))).putOpt(key, value);
                }
            } else {
                if (Utils.isEmpty(key)) {
                    if (Utils.isEmpty(position)) {
                        buffer = ((JSONArray) root).put(value);
                    } else {
                        buffer = ((JSONArray) root).put(Integer.parseInt(position), value);
                    }
                } else {
                    buffer = ((JSONObject) ((JSONArray) root).get(Integer.parseInt(position))).putOpt(key, value);
                }
            }
        }
    }
    
    /**
     * Filter to object by key
     * 
     * @param key
     * @throws Exception
     */
    public ResultDTO filterKeyNotNull(String key) throws Exception {
        Object root = getRoot();
        if (root instanceof JSONArray) {
            JSONArray array = (JSONArray) root;
            JSONArray store = new JSONArray();
            for (int index = 0; index < array.length(); index++) {
                Object item = array.get(index);
                if (item instanceof JSONObject) {
                    JSONObject jsonObject = (JSONObject) item;
                    String value = jsonObject.optString(key);
                    if (!Utils.isEmpty(value)) {
                        store.put(item);
                    }
                } else {
                    throw new Exception(ERROR_ARRAY_CANT_FILTER);
                }
                filter = store;
            }
        } else {
            throw new Exception(ERROR_NOT_ARRAY);
        }
        return this;
    }
    
    public ResultDTO remove(int position) throws Exception {
        Object root = getRoot();
        if (root instanceof JSONArray) {
            JSONArray array = (JSONArray) root;
            JSONArray store = new JSONArray();
            if (array.length() <= 0) {
                return this;
            }
            for (int index = 0; index < array.length(); index++) {
                Object item = array.get(index);
                if (index != position) {
                    store.put(item);
                }
            }
            if (buffer == null) {
                object = store;
            } else {
                buffer = store;
            }
        } else {
            throw new Exception(ERROR_NOT_ARRAY);
        }
        return this;
    }
    
    /*
     * **********************************************************************
     * Function
     * **********************************************************************
     */
    private void convert(String jsonString) throws Exception {
        if (Utils.isEmpty(jsonString)) {
            throw new Exception(ERROR_JSON_EMPTY);
        }
        
        /* Create Object holder JSON after parser */
        Object root = parse(jsonString);
        
        if (root instanceof JSONObject) {
            tag = Utils.getStringFromJson(root, "tag");
            code = Utils.getStringFromJson(root, "code");
            messenger = Utils.getStringFromJson(root, "message");
            
            object = parse(((JSONObject) root).optString("response"));
        } else {
            /* Otherwise */
            object = root;
        }
    }
    
    /**
     * @param object
     * @return
     * @throws Exception
     */
    private Object validateReturnObject(Object object) throws Exception {
        if (object instanceof String) {
            return object;
        } else if (object instanceof Integer) {
            return object + "";
        } else if (object instanceof Boolean) {
            return object;
        } else if (object instanceof JSONObject || object instanceof JSONArray) {
            return object;
        } else if (object instanceof String[]) {
            return object;
        } else {
            return object;
        }
    }
    
    private Object getRoot() throws Exception {
        Object root = null;
        if (buffer == null) {
            root = object;
        } else {
            root = buffer;
        }
        return root;
    }
    
    private Object parse(String jsonString) throws Exception {
        Object object = null;
        try {
            object = new JSONObject(jsonString);
        } catch (Exception e) {
            try {
                object = new JSONArray(jsonString);
            } catch (Exception e2) {
            }
        }
        return object;
    }
}