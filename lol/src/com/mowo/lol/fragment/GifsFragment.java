package com.mowo.lol.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.custom.volley.entity.ResultDTO;
import com.custom.volley.utils.Logger;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.mowo.funcrarygifs.R;
import com.mowo.lol.adapter.ImageAdapter;
import com.mowo.lol.base.BaseFragment;
import com.mowo.lol.object.ImageItem;
import com.mowo.lol.utils.Constants;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

@SuppressLint("NewApi")
public class GifsFragment extends BaseFragment {
	private final String TAG = GifsFragment.class.getCanonicalName();
	private final static String PARAM_VALUE = "param_value";
	private List<ImageItem> listImage;
	private ImageAdapter imageAdapter;
	private PullToRefreshListView listview;
	private int loadMore = 0;
	private LinearLayout layoutLoading;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.gifs_fragment, container,
				false);
		listview = (PullToRefreshListView) rootView
				.findViewById(R.id.timelineListview);
		layoutLoading = (LinearLayout) rootView
				.findViewById(R.id.layoutLoading);
		listImage = new ArrayList<>();
		imageAdapter = new ImageAdapter(getActivity(), listImage);
		listview.setAdapter(imageAdapter);

		String name = getArguments().getString(PARAM_VALUE);
		getData(0, name);
		listview.setOnRefreshListener(new LoadMore(name));
		return rootView;
	}

	public static GifsFragment newInstance(String value) {
		GifsFragment f = new GifsFragment();
		Bundle bundle = new Bundle();
		bundle.putString(PARAM_VALUE, value);
		f.setArguments(bundle);
		return f;
	}

	private class LoadMore implements OnRefreshListener2<ListView> {
		private String host;

		public LoadMore(String host) {
			this.host = host;
		}

		@Override
		public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
			listImage.clear();
			getData(0, host);
		}

		@Override
		public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
			loadMore = loadMore + 10;
			getData(loadMore, host);
		}
	}

	// call api
	private void getData(int page, String name) {
		loadMore = page;
		String url = Constants.BASE_URL + name + Constants.HOST
				+ "/posts?api_key=" + Constants.API_KEY + "&limit="
				+ Constants.PAGE_ITEM + "&offset=" + String.valueOf(page);
		getRequest(url, TAG, null);
	}

	// response
	@Override
	public void onResponse(ResultDTO resultDTO) throws Exception {
		super.onResponse(resultDTO);
		try {
			layoutLoading.setVisibility(View.GONE);
			listview.onRefreshComplete();
			JSONArray arrPost = (JSONArray) resultDTO.clear().key("response")
					.key("posts").value();
			int size = arrPost.length();
			for (int i = 0; i < size; i++) {
				ImageItem item = new ImageItem();
				String type = (String) resultDTO.clear(arrPost)
						.key("type", "" + i).value();
				String caption = (String) resultDTO.clear(arrPost)
						.key("caption", "" + i).value();
				String timetamp = (String) resultDTO.clear(arrPost)
						.key("timestamp", "" + i).value();
				Logger.i("ninh", "type: " + type);
				if (type.equals("photo")) {
					JSONObject originalSize = (JSONObject) resultDTO
							.clear(arrPost).key("photos", "" + i)
							.key("original_size", "" + 0).value();
					String imageOrigin = originalSize.getString("url");
					item.setImageOrigin(imageOrigin);

					JSONArray previewSize = (JSONArray) resultDTO
							.clear(arrPost).key("photos", "" + i)
							.key("alt_sizes", "" + 0).value();
					if (previewSize.length() > 2) {
						String imagePreview = previewSize.getJSONObject(2)
								.getString("url");
						item.setImagePreview(imagePreview);
					} else {
						String imagePreview2 = previewSize.getJSONObject(0)
								.getString("url");
						item.setImagePreview(imagePreview2);
					}

				}
				item.setCaption(caption);
				item.setType(type);
				item.setTimetamp(timetamp);
				listImage.add(item);
				imageAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getRequest(String url, String TAG, Object ObjectReference) {
		super.getRequest(url, TAG, ObjectReference);
	}

}
