package com.mowo.recipes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class RecipeDetail extends Activity {
	private WebView content;
	private LinearLayout layoutLoading;
	public static final String PARAM_VALUE = "param_value";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		content = (WebView) findViewById(R.id.webview);
		layoutLoading = (LinearLayout) findViewById(R.id.layoutLoading);
		Intent intent = getIntent();
		String data = intent.getStringExtra(PARAM_VALUE);
		initializeWV(data);
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	private void initializeWV(String data) {
		content.getSettings().setCacheMode(WebSettings.LOAD_NORMAL);
		content.getSettings().setJavaScriptEnabled(true);
		content.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		content.setWebViewClient(new ForumWebViewClient());
		content.loadUrl(data);
	}

	private class ForumWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			super.onLoadResource(view, url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			layoutLoading.setVisibility(View.GONE);
		}
	}

	private String getHtml(String url) throws ClientProtocolException,
			IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpGet httpGet = new HttpGet(url);
		HttpResponse response = httpClient.execute(httpGet, localContext);
		String result = "";

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				response.getEntity().getContent()));

		String line = null;
		while ((line = reader.readLine()) != null) {
			result += line + "\n";
		}
		return result;
	}
}
